#!/usr/bin/env bash
##
## Utilitary bash functions.
##

#
# GLOBALS
#
FIR_LIB='/root/builds/fir-build/lib'
LIB_OPENBLAS='/root/builds/OpenBLAS/install/lib'
DEF="-DLARGE_DATASET -DPOLYBENCH_TIME"
DIVIDER="======================================================================"
SUBDIVIDER="-------------------------------------------"
RESULT=""
POLY_UTILS="polybench-fortran-1.0/utilities"


#
# CODE
#

# Create required directories.
mkdir -p bin/ csv/ tmp/


function logKernel()
{
  echo $DIVIDER
  echo "    $1"
  echo $SUBDIVIDER
}


function logInfo()
{
  echo "[INFO] $1"
}


# Compiles a PolyBench utilitaries library.
#
function buildPolybenchUtils()
{
  rm $POLY_UTILS/fpolybench.o
  gcc -c -O3 $POLY_UTILS/fpolybench.c -o $POLY_UTILS/fpolybench.o $DEF 
}


# Compiles a Fotran input file using FIR's toolchain.
#
# $1 - Input file to be compiled (must be .f90 or .mlir)
# $2 - Output binary file path
#
function FIRBinary()
{
  # Fetch input file extension.
  extension=${1##*.}

  # Input is source code: lower to MLIR.
  if [[ "${extension^^}" == "F90" ]]
  then

    # Digest preprocessor directives.
    flang $1 -E $DEF > tmp/input.f90

    # Lower Fortran to MLIR.
    bbc tmp/input.f90 -emit-fir -o tmp/input.mlir

  # Input is already MLIR: copy and rename input file.
  elif [[ "${extension^^}" == "MLIR" ]]
  then
    cp $1 tmp/input.mlir
  fi

  # Lower MLIR to LLVM IR.
  tco tmp/input.mlir -o tmp/input.ll

  # Lower LLVM IR to assembly.
  llc tmp/input.ll -relocation-model=pic -O3 -filetype=obj -o tmp/input.o

  # Fix FIR symbols to match GCC.
  objcopy tmp/input.o \
    --redefine-sym _QPcheck_err=check_err_ \
    --redefine-sym _QPpolybench_timer_start=polybench_timer_start_ \
    --redefine-sym _QPpolybench_timer_stop=polybench_timer_stop_ \
    --redefine-sym _QPpolybench_timer_print=polybench_timer_print_

  # Fix FIR BLAS symbols to match BLAS library.
  objcopy tmp/input.o \
    --redefine-sym _QPdgemm=dgemm_ \
    --redefine-sym _QPdgemv=dgemv_ \
    --redefine-sym _QPdsymm=dsymm_ \
    --redefine-sym _QPdsyr2k=dsyr2k_ \
    --redefine-sym _QPdtrmm=dtrmm_

  # Link GCC polybench utils with FIR kernel.
  clang tmp/input.o \
    ./$POLY_UTILS/fpolybench.o \
    -lFortran_main \
    -lFortranRuntime \
    -lFortranDecimal \
    -lopenblas \
    -lpthread -lstdc++ -lm -o $2
}


# Compiles a Fotran input file and rewrites it with the specified kernel.
#
# $1 - Input file to be compiled
# $2 - PAT file to be used
# $3 - Optimized binary file path
#
function SMRBinary ()
{
  # Process compiler directives with flang.
  flang -I$POLY_UTILS $1 -E $DEF > tmp/input.f90

  # Replace polybench kernel with BLAS call using SMR.
  smr tmp/input.f90 $2 -o tmp/rewritten.mlir

  # Compile rewritten MLIR code with FIR toolchain.
  FIRBinary "tmp/rewritten.mlir" "$3"

  # Log optimized output.
  logInfo "SMR optimized binary created: $3"
}


# Compiles a Polybench Fortran input file using GFortran.
#
# $1 - Input file to be compiled
# $2 - Output binary file path
#
function gFortranBinary()
{
  gfortran $DEF -I$POLY_UTILS $POLY_UTILS/fpolybench.o \
    -ffree-line-length-none -O3 \
    $1 -o $2

  # Log gfortran binary.
  logInfo "GFortran binary created: $2"
}


# Compiles a Polybench Fortran input file using Flang.
#
# $1 - Input file to be compiled.
# $2 - Output binary file path.
#
function FlangBinary()
{
  flang $DEF -O3 \
    $POLY_UTILS/fpolybench.o \
    $1 -o $2 -I$POLY_UTILS

  # Log Flang binary.
  logInfo "Flang binary created: $2"
}


# Compiles a Polybench Fortran input file using Flang
#
# $1 - Compilation path to be timed (flang or smr)
# $2 - Kernel compilation to be timed
#
function timeExecution()
{
  # set argument variables
  input_file="polybench-fortran-1.0/linear-algebra/kernels/$2/$2.F90"
  pattern_file="patterns/$2.pat"
  binary_file="tmp/bin"

  # chose which compilation to time
  if [[ "$1" == "fir" ]]
  then command="FIRBinary $input_file $binary_file"
  elif [[ "$1" == "smr" ]]
  then command="SMRBinary $input_file $pattern_file $binary_file"
  fi

  # time compilation command and print to stdout
  ts=$(date +%s%N)
  OUTPUT=$($command)
  echo "scale=7; ($(date +%s%N) - $ts)/1000000" | bc
}


# Executes polybench time_benchmark and appends output time in a CSV format.
#
# $1 - Executable file to be timed by polybench's time_benchmark.sh.
#
function polybenchTimeToCsv()
{
  # Set polybench benchmark script.
  BENCHMARK="$POLY_UTILS/time_benchmark.sh"

  # Set regex to fetch the benchmark's output.
  REGEX="Normalized time: ([0-9.]+)"

  # Print and capture benchmark output.
  exec 5>&1
  result=$(stdbuf -oL ./$BENCHMARK $1 2> /dev/null | tee >(cat - >&5))

  # Find and append benchmark time to variable.
  if [[ "$result" =~ $REGEX ]]
  then
    TIMINGS="$TIMINGS,${BASH_REMATCH[1]}"
  fi
}
