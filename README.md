# SMR Environment

This repository contains a pre-built environment to test SMR.

## Repository Contents

### Folders

- `patterns/` - SMR PAT files used to rewrite Polybench kernels.
- `gnu-scripts/` - Gnuplot scripts for plotting CSV files.
- `patches/` - Patches for PolyBench to work with FIR.

### Bash Scripts

- `utils.sh` - Utilitary bash functions used by other scripts.
- `validate.sh` - Compares Polybench gFortran reference results against SMR results.
- `execution_times.sh` - Generates a plot comparing gFortran, Flang, and SMR benchmark times.
- `compilation_times.sh` - Generates a plot evaluating SMR rewrite time overhead.

### Others

- `Dockerfile` - Contains all steps to recreate the container.
- `compare.py` - Python script to compare relative error when validating outputs.

## Running Pre-Built Container

A pre-built container is available on Docker Hub. To run it:

```bash
docker pull sitio/smr-artifact:2023-06-07
docker run -it --name smr-container sitio/smr-artifact:2023-06-07 bash
```

## Building the Container

***Tested with docker client and engine `24.0.2`***

```bash
git clone --recurse-submodules https://gitlab.com/parlab/smr-environment.git
cd smr-environment
docker build . -t your/smr-environment
docker run -it --name smr-container your/smr-environment bash
```

Some stages of the build can use large amounts of memory depending on the number of threads utilized.

## Execution Time

SMR rewrites PolyBench Fortran kernels by using the PAT language files in `patterns/`. These rewrites use highly optimized OpenBLAS library calls, reducing execution time. To run this test:

```bash
bash execution_times.sh
```

A PDF file (`execution_times.pdf`) will be generated with a comparison plot between Flang, gFortran, and SMR codes.

## Compilation Time

SMR adds overhead to the compilation time of a program. To measure this overhead, use:

```bash
bash compilation_times.sh
```

A PDF file (`compilation_times.pdf`) will be generated with a comparison plot between Flang, gFortran, and SMR compilation times.

## Correctness

To compare reference results from Polybench against SMR's optimized results, we use a relative error comparison. The acceptable error margin is 10<sup>-7</sup>, which is equivalent to a maximum difference of 0.00001% against Polybench's original value. This test can be run with:

```bash
bash validate.sh
```
