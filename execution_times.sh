#!/bin/bash
##
## Script to plot a comparison of gFortran and SMR's execution times.
##
source utils.sh


#
# GLOBALS
#
KERNELS=("2mm" "3mm" "gemm" "syrk" "None" "atax" "bicg" "mvt") # Kernels to be timed.
DEF="-DLARGE_DATASET -DPOLYBENCH_TIME" # Polybench flags.

# Binaries folders.
GFOR_DIR="bin/gfortran"
SMR_DIR="bin/smr"
FLANG_DIR="bin/flang"


#
# SCRIPT
#

# Prepare CSV output file.
echo '"Kernel","Flang -O3","SMR+BLAS","gFortran -O3"' > csv/execution_times.csv

# Build Polybench utils library.
buildPolybenchUtils

# Time each kernel.
for kernel in ${KERNELS[@]}; do

  # Blank kernel: add blank line and skip to next kernel.
  if [[ "$kernel" == "None" ]]
  then 
    echo "" >> csv/execution_times.csv
    continue
  fi

  # Valid kernel: time it.
  logKernel "Timming ${kernel^^} Kernel Binaries"
  input_file="polybench-fortran-1.0/linear-algebra/kernels/$kernel/$kernel.F90"
  mkdir -p $GFOR_DIR $FLANG_DIR $SMR_DIR
  TIMINGS="$kernel"

  # Time Flang optimized kernel.
  logInfo "Timming Flang Binary"
  FlangBinary "$input_file" "$FLANG_DIR/$kernel"
  polybenchTimeToCsv "$FLANG_DIR/$kernel"
  echo "$SUBDIVIDER"

  # Time SMR optimized kernel.
  logInfo "Timming SMR Binary"
  SMRBinary "$input_file" "patterns/$kernel.pat" "$SMR_DIR/$kernel"
  polybenchTimeToCsv "$SMR_DIR/$kernel"
  echo "$SUBDIVIDER"

  # Time gFortran optimized kernel.
  logInfo "Timming gFortran Binary"
  gFortranBinary "$input_file" "$GFOR_DIR/$kernel"
  polybenchTimeToCsv "$GFOR_DIR/$kernel"

  # Append kernel times to CSV file.
  echo "$TIMINGS" >> csv/execution_times.csv
  echo "$DIVIDER"
done

# Plot results.
gnuplot gnu-scripts/execution_times.gnu
logInfo "Execution times plot created!"

echo "$DIVIDER"
