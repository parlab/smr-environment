#!/usr/bin/env bash

#
# IMPORTS
#
source utils.sh


#
# GLOBALS
#
KERNELS=("2mm" "3mm" "gemm" "syrk" "atax" "bicg" "mvt")
DEF="-DLARGE_DATASET -DPOLYBENCH_DUMP_ARRAYS"
GFOR_DIR="bin/validate/gfortran"
TEST_DIR="bin/validate/smr"


#
# SCRIPT
#

# Create required directories.
mkdir -p tmp/

# Build utils library (uses utils.sh method).
buildPolybenchUtils

# Validate correctnes of each SMR kernel rewrite.
for kernel in ${KERNELS[@]}; do
  input_file="polybench-fortran-1.0/linear-algebra/kernels/$kernel/$kernel.F90"
  mkdir -p $TEST_DIR $GFOR_DIR
  logKernel "Validating SMR optimized FIR ${kernel^^} Kernel"

  # Generate SMR optimized binary and fetch output.
  SMRBinary "$input_file" "./patterns/$kernel.pat" "$TEST_DIR/$kernel"
  logInfo "Generating ${kernel^^} SMR optmized output to be validated ($TEST_DIR/$kernel.txt)"
  ./$TEST_DIR/$kernel 2> $TEST_DIR/$kernel.txt
  echo $SUBDIVIDER

  # Generate gfortran reference binary and fetch output.
  logInfo "Generating reference gFortran output ($GFOR_DIR/$kernel.txt)"
  gFortranBinary "$input_file" "$GFOR_DIR/$kernel"
  ./$GFOR_DIR/$kernel 2> ./$GFOR_DIR/$kernel.txt
  echo $SUBDIVIDER

  # Check if outputs are equal.
  logInfo "Comparing outputs..."
  python3 compare.py $TEST_DIR/$kernel.txt $GFOR_DIR/$kernel.txt

done

echo $DIVIDER
