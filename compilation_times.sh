#!/bin/bash
##
## Script to generate a CSV with SMR's compilation time overhead.
##
source utils.sh


#
# GLOBALS
#
KERNELS=("2mm" "3mm" "atax" "bicg" "gemm" "mvt" "syrk")
DEF="-DLARGE_DATASET -DPOLYBENCH_TIME"
SCRIPT_PATH="tmp/compile.sh"


#
# SCRIPT
#


# Generates a temporary script which calls timeExecution utilitary method.
#
# $1 - Compilation path to be timed (flang or smr).
# $2 - Kernel compilation to be timed.
#
function generateTempScript()
{
  # Dump template into temporary file.
  printf "#!/bin/bash \nsource utils.sh \ntimeExecution " > $SCRIPT_PATH

  # Append parameters to timeExecution utilitary method.
  echo "$1 $2;" >> $SCRIPT_PATH

  # Fix permissions.
  chmod 777 $SCRIPT_PATH
}

# Prepare CSV output file.
echo '"Kernel","Flang","Flang+SMR"' > csv/compilation_times.csv

# Build utils library.
buildPolybenchUtils

# time each kernel
for kernel in ${KERNELS[@]}; do
  logKernel "Timing Compilation for ${kernel^^} Kernel"
  TIMINGS="$kernel"

  # Time raw FIR compilation.
  logInfo "Timing FIR Compilation Only"
  generateTempScript "fir" "$kernel"
  polybenchTimeToCsv "$SCRIPT_PATH"
  echo $SUBDIVIDER

  # Time FIR compilation with SMR rewrite.
  logInfo "Timing FIR Compilation with SMR Rewrite Overhead"
  generateTempScript "smr" "$kernel"
  polybenchTimeToCsv "$SCRIPT_PATH"

  # Append kernel time to CSV file.
  CSV="csv/compilation_times.csv"
  echo "$TIMINGS" >> $CSV
done

# Plot results.
gnuplot gnu-scripts/compilation_times.gnu
logInfo "Compilation overhead graph created!"

echo "$DIVIDER"
