#
# BUILD DEPS IMAGE
#
FROM ubuntu:22.10 as dependencies

# install necessary tools for the image
RUN export DEBIAN_FRONTEND=noninteractive \
 && apt-get update && apt-get install -y \
 bc \
 clang-15 \
 cmake \
 gfortran \
 git \
 gnuplot \
 libboost-serialization-dev \
 lld-15 \
 ninja-build \
 python3-pip \
 valgrind \
 wget

# fix other dependencies
RUN pip install lit
RUN ln -s /usr/bin/FileCheck-15 /usr/bin/FileCheck

#
# LLVM BUILD
#
FROM dependencies as llvm

# LLVM repository information
ARG LLVM_BRANCH='llvmorg-15.0.7'
ARG LLVM_REPO='https://github.com/llvm/llvm-project.git'
ARG LLVM_INST_DIR='/usr/local'

# clone LLVM and checkout to desired commit
RUN git clone --depth 1 -b ${LLVM_BRANCH} ${LLVM_REPO} \
 && cd llvm-project

WORKDIR /llvm-project

# Build LLVM
RUN mkdir build && cd build \
 && cmake -G Ninja ../llvm/ \
          -DCMAKE_BUILD_TYPE=Release \
          -DCMAKE_CXX_COMPILER=clang++-15 \
          -DCMAKE_C_COMPILER=clang-15 \
          -DCMAKE_INSTALL_PREFIX=${LLVM_INST_DIR} \
          -DLLVM_ENABLE_ASSERTIONS=ON \
          -DLLVM_ENABLE_LLD=ON \
          -DLLVM_ENABLE_PROJECTS="clang;mlir;flang" \
          -DLLVM_ENABLE_RTTI=ON \
          -DLLVM_TARGETS_TO_BUILD="X86" \
 && ninja install

# Use flang-new as the default flang.
RUN ln -s ${LLVM_INST_DIR}/bin/flang-new ${LLVM_INST_DIR}/bin/flang


#
# OPENBLAS BUILD
#
FROM dependencies as openblas

# Build OpenBLAS 0.3.20.
RUN git clone --depth 1 -b v0.3.20 https://github.com/xianyi/OpenBLAS.git \
 && cd OpenBLAS && mkdir install \
 && make && make PREFIX=/usr/local install



#
# FINAL IMAGE
#
FROM dependencies

# Fetch artificats from previous stages.
COPY --from=llvm /usr/local /usr/local
COPY --from=openblas /usr/local /usr/local

# Ensure dynamic libraries are found.
ENV LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

# Add artifact repo to container.
ADD ./ /root

# Move into root directory.
WORKDIR /root

# Install polybench fortran benchmark.
ARG POLYBECH_FORTRAN="http://web.cse.ohio-state.edu/~pouchet.2/software/polybench/download/polybench-fortran-1.0.tar.gz"
RUN wget -q -nc --no-check-certificate ${POLYBECH_FORTRAN} && \
    tar -x -f polybench-fortran-1.0.tar.gz

# Apply Polybench patches.
RUN for i in patches/*.patch; do patch -p0 < $i; done;

# Move into SMR build dir.
WORKDIR /root/smr/build

# Configure and build SMR frontend
RUN cmake -G Ninja .. \
          -DCMAKE_BUILD_TYPE=Release \
          -DCMAKE_C_COMPILER=clang \
          -DCMAKE_CXX_COMPILER=clang++ \
 && ninja all

# Add executable to PATH.
RUN ln -s $(pwd)/smr/smr /usr/local/bin/smr

# Reset working directory.
WORKDIR /root

# start bash session
CMD bash
